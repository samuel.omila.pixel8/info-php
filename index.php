<?php
    class API {
        public function printName($name) {
            echo "Full Name: $name<br>";
        }
        
        public function printHobbies($hobbies) {
            echo "Hobbies:<br>";
            foreach ($hobbies as $hobby) {
                echo "$hobby<br>";
            }
        }
        
        public function printInfo($info) {
            echo "Age: {$info->age}<br>";
            echo "Email: {$info->email}<br>";
            echo "Birthday: {$info->birthday}<br>";
        }
    }

    $api = new API();

    $api->printName("Samuel L. Omila");

    $hobbies = ["Gym", "Coding", "Reading"];
    $api->printHobbies($hobbies);

    $info = (object) [
        "age" => 23,
        "email" => "samuel.omila.pixel8@gmail.com",
        "birthday" => "April 17, 2000"
    ];

    $api->printInfo($info);
?>





